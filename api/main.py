from fastapi import FastAPI, Response, status
from routers import categories
from pydantic import BaseModel
import psycopg

app = FastAPI()

# Using routers for organization
# See https://fastapi.tiangolo.com/tutorial/bigger-applications/
app.include_router(categories.router)


class ClueOut(BaseModel):
    id: int
    answer: str
    question: str
    value: int
    invalid_count: int
    canon: bool


class Message(BaseModel):
    message: str


@app.get(
    "/api/clues/{clue_id}",
    response_model=ClueOut,
    responses={404: {"model": Message}},
)
def get_clue(clue_id: int, response: Response):
    with psycopg.connect()  as conn:
        with conn.cursor() as cur:
            cur.execute(
              f"""
              SELECT id, answer, question, value, invalid_count, canon
              FROM clues
              WHERE id = %s
          --    INNER JOIN categories ON (category.id = categories.category_id)
              """,
              [clue_id]
            )
            row = cur.fetchone()
            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {'message': 'Clue not found'}
            record = {}
            for i, column in enumerate(cur.description):
                record[column.name] = row[i]
            return record
